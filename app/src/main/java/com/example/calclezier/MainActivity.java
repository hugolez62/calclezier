package com.example.calclezier;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_launcher_foreground);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
    }

    public void exit(View view) {
        finish();
        System.exit(0);
    }

    public void raz(View view) {
        ((EditText) findViewById(R.id.editTextNumber1)).getText().clear();
        ((EditText) findViewById(R.id.editTextNumber2)).getText().clear();;
        ((TextView) findViewById(R.id.resultat)).setText("");
    }

    public void calculate(View view) {
        EditText editText1 = (EditText) findViewById(R.id.editTextNumber1);
        EditText editText2 = (EditText) findViewById(R.id.editTextNumber2);

        double res;

        switch (((RadioGroup) findViewById(R.id.radioGroup)).getCheckedRadioButtonId()) {

            case R.id.plus:
                res = Double.parseDouble(editText1.getText().toString()) + Double.parseDouble(editText2.getText().toString());
                break;
            case R.id.moins:
                res = Double.parseDouble(editText1.getText().toString()) - Double.parseDouble(editText2.getText().toString());
                break;
            case R.id.multiplier:
                res = Double.parseDouble(editText1.getText().toString()) * Double.parseDouble(editText2.getText().toString());
                break;
            case R.id.diviser:
                if (Double.parseDouble(editText2.getText().toString()) == 0.0) {
                    res = 0.0;
                } else {
                    res = Double.parseDouble(editText1.getText().toString()) / Double.parseDouble(editText2.getText().toString());
                }
                break;

            default:
                res = 0.0;
                break;
        }

        ((TextView) findViewById(R.id.resultat)).setText("" + res);

    }
}